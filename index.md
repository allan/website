---
layout: page
---
# Velkommen til data.coop

Vi er en forening som har formålet, at passe på medlemmernes data. Vores kerneprincipper er 

- Privatlivsbeskyttelse
- Kryptering
- Decentralisering
- Zero-knowledge

Ud fra de kerneprincipper vil vi med tiden udbyde onlinetjenester til medlemmerne. Hovedtanken er, 
at vi som udgangspunkt stoler mere på hinanden end på "de store" som f.eks. Google, Microsoft eller Facebook.

Foreningen holdt stiftende generalforsamling i 2014 og blev genstartet i 2016. 
Foreningen arbejder nu på, at få gang i aktiviteterne. Inden længe vil du her på siden kunne læse om, 
hvad du kan få ud af et medlemsskab samt detaljer om, hvordan du kan melde dig ind og være med til at opbygge noget fedt.

| Næste arrangement                                                                                       | Bliv medlem                                         |
|---------------------------------------------------------------------------------------------------------|-----------------------------------------------------|
| Vi har generalforsamling online d. 24. marts 2021 kl. 20.00. Læs mere [her](/gf2021)                 | Vi har brug for medlemmer. [Læs mere her](/medlem). |
