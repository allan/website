---
layout: page
title: Om os
permalink: /om/
---

**data.coop** er en forening og et kooperativ, som er nystartet. Visionen
er, at vi medlemmerne i kooperativet ejer vores egne data.

Dette indebærer en del og har som konsekvens, at vi bliver nødt til at eje
vores egen infrastruktur og have indblik i og kontrol over den software,
som vi bruger til at kommunikere på nettet. Ret forsimplet betyder dette:

 * At vi ejer vores egen hardware
 * At vi kun bruger open source software

Vi går med begge ben på jorden, så for rent faktisk at kunne starte rigtigt
op, kan vi ikke stille urealistiske krav: At køre åben hardware eller
drive services såsom egne søgemaskiner er således ikke inden for rammerne.
Til gengæld regner vi med at drive en række af de fede open source projekter,
som allerede findes til e-mail, kalender, dokumentdeling og SOME.

Vi ønsker pr. 2018 støttemedlemmer til at dække vores omkostninger.
Foreningen råder over 2 rack servere. Vi er ved at opbygge et medlemssystem
og afprøver nogle prototyper til den fremtidige hosting og infrastruktur.

Du kan finde os på:

 * Freenode IRC, **#data.coop**.
 * Keybase, **datacoop**
 * Vores [Gitea server](https://git.data.coop/data.coop/)
