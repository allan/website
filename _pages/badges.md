---
layout: page
title: Badges
in_menu: false
permalink: /tjenester/badges/
---

På [listen over vores tjenester](/tjenester/) er hver enkelt tjeneste tildelt
en række badges, der deklarerer i hvor høj grad tjenesten lever op til
[kerneprincipperne defineret i formålsparagraffen i vores
vedtægter](/_pages/vedtaegter.html#-2-form%C3%A5l). Her følger en generel
uddybning af hvad de forskellige badges dækker over. Der kan være små
afvigelser fra den generelle definition af hvad et badge dækker over. I disse
tilfælde vil dette blive særdeklareret under listen af badges ved en tjeneste.

## Badge-status

Et badge kan have 3 forskellige statuser:

<h3 class="badge badge-positive badge-dummy">Positiv</h3>

Tjenesten lever op til kravene til det badget dækker over.

<h3 class="badge badge-partial badge-dummy">Delvis</h3>

Tjenesten lever delvist op til det badget dækker over. Dog i en sådan grad at
vi mener det er værd at fremhæve.

<h3 class="badge badge-negative badge-dummy">Negativ</h3>

Tjenesten lever _ikke_ op til det badget dækker over.


## Detaljer om de forskellige badges

<h3 id="stabilitet" class="badge badge-stable">Stabilitet</h3>

Det første badge er som sådan ikke relateret til hvorvidt tjenesten lever op
til vores formålsparagraf. Det angiver i stedet hvorvidt tjenesten er stabil
nok til at vi vil anbefale almindelige mennesker at bruge den. Vi tilbyder
løbende nye tjenester, og der vil være en periode i starten af en tjenestes
levetid hvor vi anser den som værende "ustabil", indtil den har vist sit værd
med en mindre skare af brugere der er med på at tjenesten kan være ustabil.
Alle er dog stadig velkomne til at benytte sig af tjenesten, så længe de er
indforstået med dette forbehold for stabiliteten.

<p class="badge-credits">Grafikken er <a href="https://thenounproject.com/term/traffic-light/1925528/"><em>Traffic Light</em></a> af icon 54 fra the Noun Project.</p>


<h3 id="sikker-forbindelse" class="badge badge-secure-connection">Sikker forbindelse</h3>

Sikker forbindelse angiver at trafikken over internettet mellem dig og
tjenesten er krypteret. Du kender det som regel som en hængelås foran
adresselinjen i din browser. Det betyder at ingen andre end dig og tjenesten
kan se hvad der bliver sendt frem og tilbage mellem dig og tjenesten.

<p class="badge-credits">Grafikken er <a href="https://thenounproject.com/search/?q=transfer&i=37030"><em>transfer</em></a> af Gonzalo Bravo fra the Noun Project.</p>


<h3 id="krypteret-opbevaring-af-data" class="badge badge-encrypted-data-storage">Krypteret opbevaring af data</h3>

Tjenestens data gemmes krypteret, og er dermed ikke umiddelbart læsbare for
andre end tjenesten selv og data.coops systemadministratorer. I tilfælde af at
data lækkes (fx ved virtuelt eller fysisk indbrud), kræver det en særlig
"hovednøgle" at læse data, som datatyven forhåbentligt ikke er i besiddelse af.

<p class="badge-credits">Grafikken er <a href="https://thenounproject.com/search/?q=safe&i=821403"><em>safe</em></a> af Maxim Kulikov fra the Noun Project.</p>


<h3 id="zero-knowledge" class="badge badge-zero-knowledge">Zero knowledge</h3>

Data gemmes krypteret, og tjenesten er konstrueret på en måde, så selv ikke
data.coops systemadministratorer har mulighed for at tilgå og læse brugernes
data. Der findes simpelthen ingen "hovednøgle" som giver fuld datadgang, og det
er dermed kun brugerne selv der kan læse deres data. I tilfælde af at data
lækkes, er ingen større katastrofe sket, da der heller ikke er risiko for at
datatyven også har stjålet hovednøglen, da der ingen hovednøgle er.

<p class="badge-credits">Grafikken er <a href="https://thenounproject.com/search/?q=shrug&i=1221199"><em>Shrug</em></a> af Andrew Doane fra the Noun Project.</p>


<h3 id="backup" class="badge badge-backup">Backup</h3>

Der tages løbende backup af tjenestens data, for at minimere skadens omfang i
tilfælde af tekniske nedbrud.

**Bemærk**: Vi tager pt. slet ikke backup af noget, men det står selvfølgelig
højt på vores prioritetsliste.

<p class="badge-credits">Grafikken er <a href="https://thenounproject.com/search/?q=backup&i=2120922"><em>backup</em></a> af Adrien Coquet fra the Noun Project.</p>


<h3 id="logging" class="badge badge-logging">Logning</h3>

Dette badge er lidt omvendt: Det positive badge tildeles tjenester der _ikke_
foretager logning.

Logning fungerer på forskellig vis og foretages i større eller mindre grad. Som
regel giver logning data.coops systemadministratorer overordnet indsigt i
_hvordan_ tjenester bruges, men _ikke_ i det konkrete data tjenesten behandler
for brugerne. Fx vil logning afsløre overfor data.coops systemadministratorer,
at nogen overfører en fil, men ikke hvad selve filen indeholder eller hvem der
overfører filen.

<p class="badge-credits">Grafikken er <a href="https://thenounproject.com/search/?q=foot%20print&i=1677273"><em>Dog Paws</em></a> af iejank fra the Noun Project.</p>


<h3 id="anonym-adgang" class="badge badge-anonymous-access">Anonym adgang</h3>

Tjenester med anonym adgang kan benyttes helt uden at man skal være registreret
ved tjenesten. De fleste tjenester kræver en form for registrering af brugerne,
da de data tjenesten indeholder knyttes til de enkelte brugere. Nogle tjenester
tilbyder anonym læseadgang. Fx kan det kræve registrering at uploade en fil til
tjenesten, men andre kan downloade filen at være registreret.

<p class="badge-credits">Grafikken er <a href="https://thenounproject.com/search/?q=anonymous&i=1916559">anonymous<em></em></a> af pidzemleyu fra the Noun Project.</p>
