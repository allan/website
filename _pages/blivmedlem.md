---
layout: page
title: Bliv medlem
permalink: /medlem/
---

[data.coop](https://data.coop) er en demokratisk forening. Medlemmer bestemmer
over foreningens generelle drift og hvordan data skal forvaltes. Dette sker som
i en helt klassisk forening i henhold til vores vedtægter på den årlige
generalforsamling.

Du bliver medlem ved at overføre penge til vores konto og sende en email til
vores kasserer, [benjaoming@data.coop](mailto:benjaoming@data.coop), med dit navn.

 * Reg. 8401 (Merkur)
 * Kontonr. 1016866 
 * Tekst på overførslen: Fornavn+efternavn

Årligt kontingent: **300 kr** (dog gerne mere)<br>
(nedsat kontingent er 50 kr. for studerende/arbejdsløse)

Kontingent-perioden gælder for det år, man melder sig ind i.

Vedtægter og Acceptable Usage Policy, og deres versionshistorik, kan læses i vores [git repository](https://git.data.coop/data.coop/dokumenter).
Læs her vores [vedtægter](https://git.data.coop/data.coop/dokumenter/src/branch/master/Vedtaegter.md) og [AUP](https://git.data.coop/data.coop/dokumenter/src/branch/master/Acceptable%20Usage%20Policy.md).
